#pragma once
#include <string>
#include "Texture.h"
class Material
{
public:
	Material();
	~Material();
	void SetBaseColor(std::string texturePath);
	void SetMetallic(std::string texturePath);
	void SetNormalMap(std::string texturePath);
	void SetRoughness(std::string texturePath);
	void BindMaterial();
private:
	Texture* baseColor;
	Texture* metallic;
	Texture* normalMap;
	Texture* roughness;
};

