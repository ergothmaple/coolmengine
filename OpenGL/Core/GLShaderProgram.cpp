#include "GLShaderProgram.h"
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

GLShaderProgram::GLShaderProgram(const std::string& a_vertexShader, const std::string& a_fragmentShader)
{
    std::string parsedVertexShader = ParseShader(a_vertexShader);
    std::string parsedFragmentShader = ParseShader(a_fragmentShader);

    program = glCreateProgram();

    vertexShader = CompileShader(GL_VERTEX_SHADER, parsedVertexShader);
    fragmentShader = CompileShader(GL_FRAGMENT_SHADER, parsedFragmentShader);

    glAttachShader(program, vertexShader);
    glAttachShader(program, fragmentShader);

    glLinkProgram(program);
    glValidateProgram(program);

    glDetachShader(program, vertexShader);
    glDetachShader(program, fragmentShader);


    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);

}

GLuint GLShaderProgram::CompileShader(GLuint type, const std::string& source)
{
    GLuint id = glCreateShader(type);
    const char* src = source.c_str();
    glShaderSource(id, 1, &src, nullptr);
    glCompileShader(id);

    int result;
    glGetShaderiv(id, GL_COMPILE_STATUS, &result);

    //Error catching
    if (result == GL_FALSE)
    {
        int length;
        glGetShaderiv(id, GL_INFO_LOG_LENGTH, &length);
        char* message = (char*)_malloca(length * sizeof(char));
        glGetShaderInfoLog(id, length, &length, message);
        std::cout << "Failed to Compile " <<
            (type == GL_VERTEX_SHADER ? "Vertex" : "fragment")
            << "shader : \r\n" << message << std::endl;
        glDeleteShader(id);
        return 0;
    }

    return id;
}

std::string GLShaderProgram::ParseShader(const std::string& filepath)
{
    std::ifstream stream(filepath);
    std::string line;
    std::string result;
    std::ifstream in(filepath, std::ios::in | std::ios::binary);
    if (in)
    {
        in.seekg(0, std::ios::end);
        result.resize((size_t)in.tellg());
        in.seekg(0, std::ios::beg);
        in.read(&result[0], result.size());
        in.close();
    }
    return result;
}

GLuint GLShaderProgram::GetProgram()
{
    return program;
}