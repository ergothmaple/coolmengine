#pragma once
#include "GLShaderProgram.h"
#include "Material.h"
class Plane
{
public:
	Plane(std::string texPath);
	~Plane();
    void BindBuffers();
    void Render();

private:
    //Plane
    GLfloat vertices[32] =
    {	// vertex				texCoords				BaseColor
        1.f,  1.f, 0.0f,		1.0f, 1.0f,	            1.0f, 0.0f, 0.0f,
        1.f, -1.f, 0.0f,		1.0f, 0.0f,             0.0f, 1.0f, 0.0f,
        -1.f, -1.f, 0.0f,		0.0f, 0.0f,	            0.0f, 0.0f, 1.0f,
        -1.f,  1.f, 0.0f,		0.0f, 1.0f,             1.0f, 1.0f, 0.0f
    };

    unsigned int indices[6] = {  // note that we start from 0!
        0, 1, 3,   // first triangle
        1, 2, 3    // second triangle
    };
    Material* material;
    GLuint VBO, VAO, EBO;
};
