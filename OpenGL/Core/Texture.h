#pragma once
#include <string>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "stb_image.h"
class Texture
{
public:
	Texture(std::string texturePath);
	~Texture();
	void BindColorTexture();
	void BindNormalMap();

private:
	int width, height, channels;
	GLuint internalFormat;
	GLuint dataFormat;
	GLuint id;
	stbi_uc* data;
};
