#define TINYOBJLOADER_IMPLEMENTATION

#include "Application.h"

#include <iostream>
//Library includes
#include <gtc/matrix_transform.hpp>

//Project includes
#include "GLShaderProgram.h"
#include "Camera.h"
#include "Cube.h"
#include "Model.h"
#include "Input.h"
#include "Plane.h"

Application* Application::application = nullptr;

#ifdef _DEBUG
void GLAPIENTRY DebugMessageCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam)
{
    if (severity != GL_DEBUG_SEVERITY_NOTIFICATION)
    {
        std::cout << "OpenGL error. " << severity << " message:" << message << std::endl;
    }
}
#endif


Application::Application()
{
}

Application::~Application()
{
}
int Application::Run()
{

    if (!InitializeWindow())
    {
        return -1;
    }

    //Create Camera
    Camera camera;
    camera.SetActive();

    //Create input for moving around camera
    Input input;

    //Model gun("colt_1911.obj");
    //gun.Initialize();

    Model sphere("sphere.obj");
    sphere.Initialize();
  
    GLShaderProgram shaderProgram("resources/shaders/Vertex.shader", "resources/shaders/Fragment.shader");

    glUseProgram(shaderProgram.GetProgram());


    //Lights
    glm::vec3 lightPos0(6.f, 2.f, 10.f);

    Plane plane("C:/Users/thijs/Documents/coolmengine/OpenGL/resources/Models/brickwall");

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window))
    {
        //Calc deltatime
        double currentTime = glfwGetTime();
        float deltatime = float(currentTime - lastTime);
        lastTime = currentTime;

        glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

        /* Render here */
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Input
        input.ProcessInput(window, deltatime);
        GLuint MatrixID = glGetUniformLocation(shaderProgram.GetProgram(), "MVP");
        glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &Camera::GetActive()->GetMVP()[0][0]);

        glUniformMatrix4fv(glGetUniformLocation(shaderProgram.GetProgram(), "ModelMatrix"), 1, GL_FALSE, &Camera::GetActive()->GetModel()[0][0]);

        glUniform3fv(glGetUniformLocation(shaderProgram.GetProgram(), "lightPos0"), 1, &lightPos0[0]);
        glUniform3fv(glGetUniformLocation(shaderProgram.GetProgram(), "cameraPos"), 1, &Camera::GetActive()->GetPosition()[0]);

        glUniform1i(glGetUniformLocation(shaderProgram.GetProgram(), "baseColor"), 0);
        glUniform1i(glGetUniformLocation(shaderProgram.GetProgram(), "normalMap"), 1);

        glUniform3fv(glGetUniformLocation(shaderProgram.GetProgram(), "MV3x3"), 1, &Camera::GetActive()->GetModelView3x3()[0][0]);
        
        lightPos0.x -= 0.1f;
        //sphere.BindBuffers();
       // sphere.Render();
        plane.Render();

        /* Swap front and back buffers */
        glfwSwapBuffers(window);

        /* Poll for and process events */
        glfwPollEvents();

    }

    glDeleteProgram(shaderProgram.GetProgram());
    glfwTerminate();
    return 0;
}

bool Application::InitializeWindow()
{
    /* Initialize the GLFW */
    if (!glfwInit())
        return false;

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    /* Create a windowed mode window and its OpenGL context */
    int windowWidth = 640;
    int windowHeight = 480;
    window = glfwCreateWindow(windowWidth, windowHeight, "CoolMengine", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        return false;
    }

    /* initialize glew */
    glfwMakeContextCurrent(window);
    if (glewInit() != GLEW_OK)
        std::cout << "Glew Not properly Initialized" << std::endl;

    //limit fps
    glfwSwapInterval(5);

    std::cout << "OpenGL Version : " << glGetString(GL_VERSION) << std::endl;
    //Enable debugging
    glEnable(GL_DEBUG_OUTPUT);
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    glDebugMessageCallback(DebugMessageCallback, nullptr);


    // Enable depth test
    glEnable(GL_DEPTH_TEST);
    // Accept fragment if it closer to the camera than the former one
    glDepthFunc(GL_LESS);
}

GLFWwindow* Application::GetWindow()
{
    return nullptr;
}
