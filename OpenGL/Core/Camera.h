#pragma once
#include <gtc/matrix_transform.hpp>
class Camera
{
public:
	Camera();
	~Camera();

	void Update();


	void addRotation(const glm::vec3 addedRotation);
	void AddPos(glm::vec3 addedPosition);

	glm::mat4 GetMVP();
	glm::vec3 GetDirection();
	glm::vec3 GetUp();
	glm::vec3 GetRight();
	glm::mat4 GetModel();
	glm::mat3 GetModelView3x3();
	glm::vec3 GetPosition();
	void SetActive();
	static Camera* GetActive();

private:

	static Camera* activeCamera;

	glm::mat4 projection;
	glm::mat4 view;
	glm::mat4 model;
	glm::mat4 mvp;

	glm::vec3 direction = glm::vec3(0, 0, -5);
	glm::vec3 right;
	glm::vec3 up = glm::vec3(0, 1, 0);

	glm::vec3 position = glm::vec3(0, 0, 5);
	glm::vec3 rotation = glm::vec3(3.14f, 0.f, 0.f);

	// Initial Field of View
	float fov = 45.0f;
};