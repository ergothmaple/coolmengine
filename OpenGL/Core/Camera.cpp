#include "Camera.h"

//initialize static ptr
Camera* Camera::activeCamera = nullptr;

Camera::Camera()
{ 
    // Projection matrix : 45�� Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
    projection = glm::perspective(glm::radians(fov), 640.f / 480.f, .1f, 100.f);

    //Camera matrix
    view = glm::lookAt(
        position, 
        position+direction, 
       up 
    );

    // Model matrix : an identity matrix (model will be at the origin)
    model = glm::mat4(1.0f);
    mvp = projection * view * model; 
}

Camera::~Camera()
{
    if (activeCamera == this)
    {
        activeCamera = nullptr;
    }
}

glm::mat4 Camera::GetMVP()
{
    return mvp;
}

glm::vec3 Camera::GetDirection()
{
    return direction;
}

glm::vec3 Camera::GetUp()
{
    return up;
}

glm::vec3 Camera::GetRight()
{
    return right;
}

glm::mat4 Camera::GetModel()
{
    return model;
}

glm::mat3 Camera::GetModelView3x3()
{
    return glm::mat3(view * model);
}

glm::vec3 Camera::GetPosition()
{
    return position;
}

void Camera::AddPos(glm::vec3 addedPosition)
{
    position += addedPosition;
}

void Camera::Update()
{
    direction = glm::vec3(
        cos(rotation.y) * sin(rotation.x),
        sin(rotation.y),
        cos(rotation.y) * cos(rotation.x)
    );

    right = glm::vec3(
        sin(rotation.x - 3.14f / 2.0f),
        0,
        cos(rotation.x - 3.14f / 2.0f)
    );
    up = glm::cross(right, direction);


    // Projection matrix : 45�� Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
    projection = glm::perspective(glm::radians(fov), 640.f / 480.f, .1f, 100.f);

    //Camera matrix
    view = glm::lookAt(
        position, // Camera is at (4,3,3), in World Space
        position + direction, // and looks at the origin
        up  // Head is up (set to 0,-1,0 to look upside-down)
    );

    // Model matrix : an identity matrix (model will be at the origin)
    model = glm::mat4(1.0f);
    // Our ModelViewProjection : multiplication of our 3 matrices
    mvp = projection * view * model; // Remember, matrix multiplication is the other way around
    

}

void Camera::SetActive()
{
    activeCamera = this;
}

void Camera::addRotation(const glm::vec3 addedRotation)
{
    rotation += addedRotation;
}

Camera* Camera::GetActive()
{
    return activeCamera;
}
