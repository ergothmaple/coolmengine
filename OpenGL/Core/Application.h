#pragma once
#include <GL/glew.h>
#include <GLFW/glfw3.h>
class Application
{
public:
	static Application* GetInstance() {
		if (!application)
		{
			application = new Application;
		}
		return application;
	};
	~Application();

	int Run();
	bool InitializeWindow();
	GLFWwindow* GetWindow();
private:
	Application();
	static Application* application;

	GLFWwindow* window;

	float lastTime = 0.f;
};
