#include "Input.h"

Input::Input()
{
}

Input::~Input()
{
}


void Input::ProcessInput(GLFWwindow* window, const float deltatime)
{
    double xPos, yPos;
    glfwGetCursorPos(window, &xPos, &yPos);

    if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT))
    {
        int xdiff = currentMousePos.first - xPos;
        int ydiff = currentMousePos.second - yPos;
        Camera::GetActive()->addRotation(glm::vec3(mouseSpeed * deltatime * float(xdiff), mouseSpeed * deltatime * float(ydiff), 0.f));
        UpdateCam = true;
    }
    currentMousePos = std::pair<double, double>(xPos, yPos);

    if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT))
    {
        if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
            Camera::GetActive()->AddPos(Camera::GetActive()->GetDirection() * deltatime * moveSpeed);
            UpdateCam = true;
        }
        // Move backward
        if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
            Camera::GetActive()->AddPos(-Camera::GetActive()->GetDirection() * deltatime * moveSpeed);
            UpdateCam = true;
        }
        // Strafe right
        if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
            Camera::GetActive()->AddPos(Camera::GetActive()->GetRight() * deltatime * moveSpeed);
            UpdateCam = true;
        }
        // Strafe left
        if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
            Camera::GetActive()->AddPos(-Camera::GetActive()->GetRight() * deltatime * moveSpeed);
            UpdateCam = true;
        }
        //Up with q
        if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS) {
            Camera::GetActive()->AddPos(Camera::GetActive()->GetUp() * deltatime * moveSpeed);
        }
        //Down with e
        if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS) {
            Camera::GetActive()->AddPos(-Camera::GetActive()->GetUp() * deltatime * moveSpeed);
        }

    }
    // glfwSetCursorPos(window, windowWidth / 2, windowHeight / 2);
    if (UpdateCam == true)
    {
        Camera::GetActive()->Update();
        UpdateCam = false;
    }
}