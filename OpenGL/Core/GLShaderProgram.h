#pragma once
#include <GL/glew.h>
#include <string>

class GLShaderProgram
{
public:
	GLShaderProgram(const std::string& a_vertexShader, const std::string& a_fragmentShader);
	~GLShaderProgram() {};


	GLuint CompileShader(GLuint type, const std::string& source);
	std::string ParseShader(const std::string& filepath);

	GLuint GetProgram();


private:
	GLuint program;
	GLuint vertexShader;
	GLuint fragmentShader;
};

