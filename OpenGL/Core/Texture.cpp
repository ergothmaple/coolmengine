#include "Texture.h"

#include <assert.h>
Texture::Texture(std::string texturePath)
{
    glGenTextures(1, &id);
    glBindTexture(GL_TEXTURE_2D, id);

    //OpenGL textures are upside down, so we need to flip them on load
    stbi_set_flip_vertically_on_load(true);

    data = stbi_load(texturePath.c_str(), &width, &height, &channels, 0);
    assert(data && "Failed to load image!");

    // RGB or RGBA?
    if (channels == 4)
    {
        internalFormat = GL_RGBA8;
        dataFormat = GL_RGBA;
    }
    else if (channels == 3)
    {
        internalFormat = GL_RGB8;
        dataFormat = GL_RGB;
    }
    assert(internalFormat & dataFormat && "Format not supported!");

    //Set some epic parameters
    glTextureParameteri(id, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTextureParameteri(id, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTextureParameteri(id, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTextureParameteri(id, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0, dataFormat, GL_UNSIGNED_BYTE, data);

}

Texture::~Texture()
{
}

void Texture::BindColorTexture()
{

    glActiveTexture(GL_TEXTURE0 + 0);
    glBindTexture(GL_TEXTURE_2D, id);

    // Put the data in the image
}

void Texture::BindNormalMap()
{
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, id);
}
