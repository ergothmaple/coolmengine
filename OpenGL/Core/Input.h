#pragma once
#include <iostream>
#include <GLFW/glfw3.h>
#include "Camera.h"
class Input
{
public:
	Input();
	~Input();
	void ProcessInput(GLFWwindow* window, const float deltatime);
private:
	std::pair<double, double> currentMousePos = std::pair<double, double>(0, 0);
	bool UpdateCam = false;
	float moveSpeed = 5.f;
	float mouseSpeed = 0.02f;
};



