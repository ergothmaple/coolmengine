#include "Material.h"

Material::Material()
{
}

Material::~Material()
{
    delete baseColor;
    delete metallic;
    delete normalMap;
    delete roughness;
}

void Material::SetBaseColor(std::string texturePath)
{
    Texture* texture = new Texture(texturePath);
    baseColor = texture;
}

void Material::SetMetallic(std::string texturePath)
{
    Texture* texture = new Texture(texturePath);
    metallic = texture;
}

void Material::SetNormalMap(std::string texturePath)
{
    Texture* texture = new Texture(texturePath);
    normalMap = texture;
}
void Material::SetRoughness(std::string texturePath)
{
    Texture* texture = new Texture(texturePath);
    roughness = texture;
}

void Material::BindMaterial()
{
    if (baseColor != nullptr)
    {
        baseColor->BindColorTexture();
    }
    if (normalMap != nullptr)
    {
        normalMap->BindNormalMap();
    }
}
