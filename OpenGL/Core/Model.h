#pragma once
#include <string>
#include "tiny_obj_loader.h"
#include <glm.hpp>
#include <GL\glew.h>
#include "Texture.h"
#include "Material.h"

class Model
{
public:
	Model(std::string a_modelName);
	~Model();
	bool Initialize();
	void BindBuffers();

	void Render();
private:
	std::string modelName;
	bool LoadObj(std::string modelName);
	void computeTangentBasis(std::vector<glm::vec3> vertices, std::vector<glm::vec2> uvs, std::vector<glm::vec3> normals);


	std::vector<glm::vec3> vertices;
	std::vector<glm::vec2> uvs;
	std::vector<glm::vec3> normals;
	std::vector<Material*> materials;

	std::vector<glm::vec3> tangents;
	std::vector<glm::vec3> bitangents;

	GLuint vertexbuffer;
	GLuint uvbuffer;
	GLuint vao; //vertex array object
	GLuint normalBuffer;
	GLuint tangentbuffer;
	GLuint biTangentbuffer;
};
