#include "Model.h"

Model::Model(std::string a_modelName)
{
    modelName = a_modelName;
}

Model::~Model()
{
    for (Material* material : materials)
    {
        delete material;
    }
}


bool Model::Initialize()
{
    if (!LoadObj(modelName))
    {
        return false;
    }
    BindBuffers();
    return true;
}



bool Model::LoadObj(std::string modelName)
{
    tinyobj::attrib_t attribs;
    std::vector<tinyobj::shape_t> shapes;
    std::vector<tinyobj::material_t> a_materials;
    std::string warningLog;
    std::string errorLog;
    std::string filename = "C:/Users/thijs/Documents/coolmengine/OpenGL/resources/Models/" + modelName;
    std::string mtlDir = "C:/Users/thijs/Documents/coolmengine/OpenGL/resources/Models";
    std::string base_dir = "resources\Models";
    bool succesfullyLoaded = tinyobj::LoadObj(&attribs, &shapes, &a_materials, &warningLog, &errorLog, filename.c_str(), mtlDir.c_str());
    if (!succesfullyLoaded)
    {
        printf("Warning log: %s", warningLog.c_str());
        printf("Error log: %s", errorLog.c_str());

    }
    a_materials;
    for (unsigned int s = 0; s < shapes.size(); s++)
    {


        tinyobj::shape_t shape = shapes[s];

        GLuint offset = 0;
        for (unsigned int i = 0; i < shape.mesh.num_face_vertices.size(); i++)
        {
            unsigned int faceVertices = shape.mesh.num_face_vertices[i];

            for (unsigned int j = 0; j < faceVertices; j++)
            {
                tinyobj::index_t index = shape.mesh.indices[offset + j];
                unsigned int vertexIndex = index.vertex_index;
                unsigned int uvIndex = index.texcoord_index;
                unsigned int normalIndex = index.normal_index;

                glm::vec3 vertex;
                vertex.x = attribs.vertices[3 * vertexIndex + 0];
                vertex.y = attribs.vertices[3 * vertexIndex + 1];
                vertex.z = attribs.vertices[3 * vertexIndex + 2];
                vertices.push_back(vertex);

                glm::vec2 uv;
                uv.x = attribs.texcoords[2 * uvIndex + 0];
                uv.y = attribs.texcoords[2 * uvIndex + 1];
                uvs.push_back(uv);

                glm::vec3 normal;
                normal.x = attribs.normals[3 * normalIndex + 0];
                normal.y = attribs.normals[3 * normalIndex + 0];
                normal.z = attribs.normals[3 * normalIndex + 0];
                normals.push_back(normal);
            }
            offset += faceVertices;
        }

    }
    std::string texDir = "C:/Users/thijs/Documents/coolmengine/OpenGL/resources/Models/";
    for (unsigned int i = 0; i < a_materials.size(); i++)
    {
        Material* material = new Material();
        material->SetBaseColor(texDir + a_materials[i].diffuse_texname);
        if (a_materials[i].bump_texname != "")
        {
            material->SetNormalMap(texDir + a_materials[i].bump_texname);
            computeTangentBasis(vertices, uvs, normals);
        }
        materials.push_back(material);
    }
    return true;
}


void Model::computeTangentBasis(std::vector<glm::vec3> vertices, std::vector<glm::vec2> uvs, std::vector<glm::vec3> normals)
{
    for (int i = 0; i < vertices.size(); i += 3)
    {
        glm::vec3 v0 = vertices[i + 0];
        glm::vec3 v1 = vertices[i + 1];
        glm::vec3 v2 = vertices[i + 2];

        // Shortcuts for UVs
        glm::vec2 uv0 = uvs[i + 0];
        glm::vec2 uv1 = uvs[i + 1];
        glm::vec2 uv2 = uvs[i + 2];

        // Edges of the triangle : position delta
        glm::vec3 deltaPos1 = v1 - v0;
        glm::vec3 deltaPos2 = v2 - v0;

        // UV delta
        glm::vec2 deltaUV1 = uv1 - uv0;
        glm::vec2 deltaUV2 = uv2 - uv0;

        float r = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV1.y * deltaUV2.x);
        glm::vec3 tangent = (deltaPos1 * deltaUV2.y - deltaPos2 * deltaUV1.y) * r;
        glm::vec3 bitangent = (deltaPos2 * deltaUV1.x - deltaPos1 * deltaUV2.x) * r;

        // Set the same tangent for all three vertices of the triangle.
        // They will be merged later, in vboindexer.cpp
        tangents.push_back(tangent);
        tangents.push_back(tangent);
        tangents.push_back(tangent);

        // Same thing for bitangents
        bitangents.push_back(bitangent);
        bitangents.push_back(bitangent);
        bitangents.push_back(bitangent);

    }
}

void Model::BindBuffers()
{
    for (Material* material : materials)
    {
        material->BindMaterial();
    }
    glGenBuffers(1, &vertexbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);

    glGenBuffers(1, &uvbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
    glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);

    glGenBuffers(1, &normalBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
    glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec3), &normals[0], GL_STATIC_DRAW);

    
    glGenBuffers(1, &tangentbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, tangentbuffer);
    glBufferData(GL_ARRAY_BUFFER, tangents.size() * sizeof(glm::vec3), &tangents[0], GL_STATIC_DRAW);

    
    glGenBuffers(1, &biTangentbuffer);
    glBufferData(GL_ARRAY_BUFFER, bitangents.size() * sizeof(glm::vec3), &bitangents[0], GL_STATIC_DRAW);

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

}

void Model::Render()
{
    // Send our transformation to the currently bound shader, in the "MVP" uniform
        // This is done in the main loop since each model will have a different MVP matrix (At least for the M part)


    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glVertexAttribPointer(
        0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
        3,                  // size
        GL_FLOAT,           // type
        GL_FALSE,           // normalized?
        0,                  // stride
        (void*)0            // array buffer offset
    );

    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
    glVertexAttribPointer(
        1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
        2,                                // size
        GL_FLOAT,                         // type
        GL_FALSE,                         // normalized?
        0,                                // stride
        (void*)0                          // array buffer offset
    );

    glEnableVertexAttribArray(2); //normals
    glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
    glVertexAttribPointer(
        2,
        3,
        GL_FLOAT,
        GL_FALSE,
        0,
        (void*)0
    );

    glEnableVertexAttribArray(3);
    glBindBuffer(GL_ARRAY_BUFFER, tangentbuffer);
    glVertexAttribPointer(
        3,
        3,
        GL_FLOAT,
        GL_FALSE,
        0,
        (void*)0
    );

    glEnableVertexAttribArray(4);
    glBindBuffer(GL_ARRAY_BUFFER, biTangentbuffer);
    glVertexAttribPointer(
        4,
        3,
        GL_FLOAT,
        GL_FALSE,
        0,
        (void*)0
    );

    glDrawArrays(GL_TRIANGLES, 0, vertices.size()); // Starting from vertex 0; 3 vertices total -> 1 triangle

    //Reset arrays
    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(2);
    glDisableVertexAttribArray(3);
    glDisableVertexAttribArray(4);
}