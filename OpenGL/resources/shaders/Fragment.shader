#version 410 core

layout(location = 0) out vec3 color;
in vec3 fragmentColor;

in vec3 vs_Position;
in vec2 UV;
in vec3 normal;
in mat3 TBN;
in mat4 Model;

uniform vec3 lightPos0;
uniform vec3 cameraPos;
uniform sampler2D baseColor;
uniform sampler2D normalMap;

void main()
{
    vec3 lightColor = vec3(0.6f);

    //Calculate normal
    vec3 normal0 = texture(normalMap, UV).rgb;
    vec3 normal1 = normalize(normal0 * 2.0 - 1.0);
    normal1 = mat3(transpose(inverse(Model))) * normal1;

    //Ambient light
    float ambientStrength = 0.1f;
    vec3 ambient = ambientStrength * lightColor;

    //Diffuse
    vec3 diffLightDir = normalize(lightPos0 - vs_Position);
    float diff = max(dot(normal1, diffLightDir), 0.0);
    vec3 diffuse = diff * lightColor;

    //specular
    vec3 specLightDir = normalize(lightPos0 - vs_Position);
    vec3 viewDir = normalize(cameraPos - vs_Position);
    vec3 halfwayDir = normalize(specLightDir + viewDir);
    float spec = pow(max(dot(normal1, halfwayDir), 0.0), 100.f);
    vec3 specular = lightColor * spec;






    vec3 result = texture(baseColor, UV).rgb * (ambient + diffuse + specular);
    
    color = result;



  /*   //Transform normal from color to normal (from [-1, 1] to [0,1]
     vec3 rgb_normal = texture(normalMap, UV).rgb * 0.5 + 0.5;
     vec3 mapNormal = normalize(texture(normalMap, UV).rgb * 2.0 - 1.0);
     //Ambient light
     vec3 ambientLight = vec3(0.1f, 0.1f, 0.1f);
     //Diffuse light
     vec3 positionToLightDirectionVector = normalize(lightPos0 - vs_Position);
     vec3 diffuseColor = vec3(1.f, 1.f, 1.f);
     float diffuse = clamp(dot(positionToLightDirectionVector, normal), 0, 1);
     vec3 diffuseFinal = diffuseColor * diffuse;

     //specular light, might be wrong, can't see this properly on the gun model
     vec3 lightDir = normalize(lightPos0 - vs_Position);
     vec3 viewDir = normalize(cameraPos - vs_Position);
     vec3 halfwayDir = normalize(lightDir + viewDir);

     float spec = pow(max(dot(normal, halfwayDir), 0.0), 3);
     vec3 specular = diffuseColor * spec;
     //Texture color
    color = texture(baseColor, UV).rgb * (ambientLight + diffuseFinal + specular);*/
};