#version 410 core

layout(location = 0) in vec4 position;
layout(location = 1) in vec2 vertexUV;
layout(location = 2) in vec3 vertexNormal;
layout(location = 3) in vec3 tangent;
layout(location = 4) in vec3 bitangent;

out vec3 vs_Position;
out vec2 UV;
out vec3 normal;
out mat3 TBN;
out mat4 Model;
uniform mat4 MVP;
uniform mat4 ModelMatrix;
uniform mat3 MV3x3;
void main()
{
/*    gl_Position = MVP * position;
    UV = vertexUV;

    vs_Position = vec3(ModelMatrix * position);

    mat3 normalMatrix = transpose(inverse(mat3(ModelMatrix)));

    vec3 T = normalize(normalMatrix * tangent);
    vec3 B = normalize(normalMatrix * bitangent);
    vec3 N = normalize(normalMatrix * normal);
    
    mat3 TBN = transpose(mat3(T, B, N));

    Model = ModelMatrix;



    //Transform normal to world space from object space
    normal = mat3(ModelMatrix) * vertexNormal;*/
    gl_Position = MVP * position;
    UV = vertexUV;

    //vs_Position = vec3(position);

    vec3 T = normalize(vec3(ModelMatrix * vec4(tangent, 0.0)));
    vec3 B = normalize(vec3(ModelMatrix * vec4(bitangent, 0.0)));
    vec3 N = normalize(vec3(ModelMatrix * vec4(vertexNormal, 0.0)));
    mat3 TBN = transpose(mat3(T, B, N));
    Model = ModelMatrix;


    //Transform normal to world space from object space
    //normal = mat3(ModelMatrix) * vertexNormal;
    normal = vertexNormal;
    vs_Position = vec3(ModelMatrix * position);
};

